<?php
	require_once 'framework/config.php';
	require_once 'framework/base/request.php';

	abstract class Model {
		public $data;
		private $config;
		protected $db;
		protected $request;

		public function __construct(Request $request) {
			$this->request = $request;

			$this->config = Config::getInstance();
			$this->config->setProperty("dbengine", "mysql");
			$this->config->setProperty("dbserver", "localhost");
			$this->config->setProperty("dbname", "final");
			$this->config->setProperty("dbuser", "root");
			$this->config->setProperty("dbpassword", "root");

			$connection_settings = $this->config->getProperty("dbengine") . ':host=' . $this->config->getProperty("dbserver") . ';dbname=' . $this->config->getProperty("dbname") . ';charset=utf8';
			
			try {
				$this->db = new PDO($connection_settings, $this->config->getProperty("dbuser"),	$this->config->getProperty("dbpassword"));
				$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $exception) {
				$this->request->addFeedback('Nie można się połączyć z bazą danych i dokonać na niej podstawowych ustawień. ' . $exception->getMessage());
			}

		}

		protected function hashPassword($password) {
			$salt = 'N]">dTZS]r+>ac[;V=OQy9-zu"UENogS';
			return hash('sha256', $password + $salt);
		}
	}