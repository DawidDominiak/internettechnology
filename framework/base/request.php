<?php
	class Request {
		private $properties = [];
		private $feedback = [];

		function __construct() {
			$this->init();
		}

		function init() {
			if(isset($_SERVER['REQUEST_METHOD'])) {
				$this->properties = $_REQUEST;
				return;
			}
		}

		function getProperty($key) {
			if(isset($this->properties[$key])) {
				return $this->properties[$key];
			}
			return null;
		}

		function addFeedback($msg) {
			array_push($this->feedback, $msg);
		}

		function getFeedback() {
			return $this->feedback;
		}

		function hasFeedback() {
			return (count($this->feedback) > 0);
		}
	}