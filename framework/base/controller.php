<?php
	require_once 'request.php';

	abstract class Controller {
		final function __construct() {}
		protected $request;

		function execute(Request $request) {
			$this->request = $request;
			$this->doExecute();
		}

		abstract function doExecute();
	}