<?php
	class Config {
		private $properties = [];
		private static $instance;

		private function __construct() {}

		public static function getInstance() {
			if(empty(self::$instance)) {
				self::$instance = new Config();
			}
			return self::$instance;
		}

		public function getProperty($key) {
			return $this->properties[$key];
		}

		public function setProperty($key, $value) {
			$this->properties[$key] = $value;
		}
	}