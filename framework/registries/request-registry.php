<?php
	require_once '../base/request.php';

	class RequestRegistry {
		private Request $request;
		private static $instance = null;

		private function __construct() {}

		static function instance() {
			if(!is_null(self::$instance)) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		static function getRequest() {
			$instance = self::instance();
			if(!$request) {
				$this->request = new Request();
			}
			return $request;
		}
	}