<?php
	require_once 'validators/address-validator.php';
	require_once 'validators/city-validator.php';
	require_once 'validators/dynamic-validator.php';
	require_once 'validators/education-validator.php';
	require_once 'validators/email-validator.php';
	require_once 'validators/expression-validator.php';
	require_once 'validators/hobbies-validator.php';
	require_once 'validators/in-array-validator.php';
	require_once 'validators/name-validator.php';
	require_once 'validators/part-of-array-validator.php';
	require_once 'validators/password-validator.php';
	require_once 'validators/validator.php';
	require_once 'validators/zip-validator.php';