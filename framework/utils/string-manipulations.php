<?php
	function toCamelCase($string) {
		return preg_replace('/\s+/', '', ucwords(str_replace('-', ' ', $string)));
	}