<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class ZipValidator extends ExpressionValidator {
		protected function validate() {
			$this->setPattern('/^[0-9]{2}\-[0-9]{3}$/i'); //RegExp polskiego kodu pocztowego
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawnym polskim kodem pocztowym.");
			}
		}
	}