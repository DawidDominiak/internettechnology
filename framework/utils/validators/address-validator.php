<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class AddressValidator extends ExpressionValidator {
		protected function validate() {
			$this->setPattern('/^[a-żA-Ż0-9\s\/\.\:\-]+$/i'); //RegExp walidujący adres
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawnym polskim adresem.");
			}
		}
	}