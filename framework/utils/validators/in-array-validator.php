<?php
	require_once 'framework/utils/validators/validator.php';
	require_once 'framework/errors/validate-exception.php';

	class InArrayValidator extends Validator {
		protected $array = [];


		public function setArray($array) {
			$this->array = $array;
		}

		protected function validate() {

			if(in_array($this->expression, $this->array)) {
				return true;
			}

			throw new ValidateException('Wyrażenia: ' . $this->expression . ' nie ma w tablicy.');
		}
	}