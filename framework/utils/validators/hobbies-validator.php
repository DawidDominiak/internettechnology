<?php
	require_once 'framework/utils/validators/part-of-array-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class HobbiesValidator extends PartOfArrayValidator {
		protected function validate() {
			$this->setArray([
				'nauka',
				'polityka',
				'lingwistyka',
				'podróże',
				'programowanie'
			]);
			if(!is_array($this->expression)) {
				$this->expression = [];
			}
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException(join(", ", $this->expression) . ' nie jest poprawną listą zainteresowań.');
			}
		}
	}