<?php
	require_once 'framework/utils/validators/in-array-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class EducationValidator extends InArrayValidator {
		protected function validate() {
			$this->setArray([
				'podstawowe',
				'średnie',
				'wyższe'
			]);
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawnym wykształceniem.");
			}
		}
	}