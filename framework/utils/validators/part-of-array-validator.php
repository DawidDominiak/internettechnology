<?php
	require_once 'framework/utils/validators/validator.php';
	require_once 'framework/errors/validate-exception.php';

	class PartOfArrayValidator extends Validator {
		protected $array = [];

		public function setArray($array) {
			$this->array = $array;
		}

		protected function validate() {
			if(count(array_intersect($this->expression, $this->array)) == count($this->expression)) {
				return true;
			}

			throw new ValidateException('Tablica: ' . join(", ", $this->expression) . ' nie zawiera się w tablicy ' . join(", ", $this->array) . '.');
		}
	}