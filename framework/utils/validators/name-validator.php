<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class NameValidator extends ExpressionValidator {
		protected function validate() {
			$this->setPattern('/^[a-żA-Ż\-]{2,40}$/i'); //Imię lub nazwisko powinny zawierać wyłącznie słowa od dwóch do 40 znaków z zakresu A-Ż, a-ż i opcjonalnie myślnik. 
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawnym imieniem lub nazwiskiem.");
			}
		}
	}