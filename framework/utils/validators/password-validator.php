<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';

	class PasswordValidator extends Validator {
		private $minimulLevelOfEntropy;

		protected function validate() {
			$currentEntropy = $this->getEntropy();

			if(!isset($this->minimulLevelOfEntropy)) {
				throw new Exception('Nie ma ustawionego minimalnego poziomu entropii.');
			}

			if($currentEntropy < $this->minimulLevelOfEntropy) {
				throw new ValidateException('Hasło jest zbyt słabe. Aktualny poziom entropii: ' . $currentEntropy . ' (wymagany: ' . $this->minimulLevelOfEntropy . ')');
			}
		}

		public function setMinimumLevelOfEntropy($level) {
			$this->minimulLevelOfEntropy = $level;
		}

		public function getEntropy() {
			return strlen($this->expression)*log10($this->getSymbolCount())/log10(2);
		}

		private function getSymbolCount() {
			$uniqueCharacters = [];
			foreach (str_split($this->expression) as $character) {
				if(!isset($uniqueCharacters[$character])) {
					$uniqueCharacters[$character] = true;
				}
			}
			return count($uniqueCharacters);
		}
	}
