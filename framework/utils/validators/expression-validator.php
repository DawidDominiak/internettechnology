<?php
	require_once 'framework/utils/validators/validator.php';
	require_once 'framework/errors/validate-exception.php';

	class ExpressionValidator extends Validator {
		protected $pattern;


		public function setPattern($pattern) {
			$this->pattern = $pattern;
		}

		protected function validate() {
			if(!preg_match($this->pattern, $this->expression)) {
				throw new ValidateException('Wyrażenie: ' . $this->expression . ' nie pasuje do wzorca ' + $this->pattern);
			}
		}
	}