<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class EmailValidator extends ExpressionValidator {
		protected function validate() {
			$this->setPattern('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\b/i'); 
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawnym emailem.");
			}
		}
	}