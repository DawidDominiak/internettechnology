<?php
	require_once 'framework/utils/validators/expression-validator.php';
	require_once 'framework/errors/validate-exception.php';
	
	class CityValidator extends ExpressionValidator {
		protected function validate() {
			$this->setPattern('/^[a-żA-Ż\s\/\.\-]+$/i'); //RegExp walidujący miasto
			try {
				parent::validate(); 
			} catch (ValidateException $exception) {
				throw new ValidateException("Wyrażenie " . $this->expression . " nie jest poprawną nazwą miasta.");
			}
		}
	}