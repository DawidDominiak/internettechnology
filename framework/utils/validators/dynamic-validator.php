<?php
	require_once 'framework/utils/validators/validator.php';
	require_once 'framework/errors/validate-exception.php';

	class DynamicValidator extends Validator {
		protected $validation_function;


		public function setFunction(Closure $validation_function) {
			$this->validation_function = $validation_function;
		}

		protected function validate() {
			$validation_function = $this->validation_function;
			return $validation_function($this->expression);
		}
	}