<?php
	require_once 'framework/errors/validate-exception.php';

	abstract class Validator {
		protected $expression;
		protected $exception = null;

		public function __construct($expression) {
			$this->expression = $expression;
		}

		public function check() {
			try {
				$this->validate();
			} catch (ValidateException $exception) {
				$this->exception = $exception;
				return false;
			}
			return true;
		}

		protected abstract function validate();

		public function getException() {
			return $this->exception;
		}
	}