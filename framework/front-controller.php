<?php
	require_once 'base/controller.php';
	require_once 'controllers/main-page.php';
	require_once 'utils/string-manipulations.php';

	class FrontController {
		private static $base_ctrl = null;
		private static $default_ctrl = null;

		function __construct() {
			if(is_null(self::$base_ctrl)) {
				self::$base_ctrl = new ReflectionClass("Controller");
				self::$default_ctrl = new MainPageController();
			}
		}

		function getCommand(Request $request) {
			$controller_name = $request->getProperty('page');
			if(!$controller_name) {
				return self::$default_ctrl;
			}
			$filepath = 'controllers/' . $controller_name . '.php';
			$classname = toCamelCase($controller_name) . 'Controller';
			if(file_exists($filepath)) {
				require_once($filepath);
				if(class_exists($classname)) {
					$ctrl_class = new ReflectionClass($classname);
					if($ctrl_class->isSubClassOf(self::$base_ctrl)) {
						return $ctrl_class->newInstance();
					} else {
						$this->sendFeedbackMsg($request, "Klasa " . $classname . " nie jest dzieckiem klasy Controller.");
					}
				} else {
					$this->sendFeedbackMsg($request, "Klasa " . $classname . " nie istnieje w pliku " . $filepath . ".");
				}
			} else {
				$this->sendFeedbackMsg($request, "Plik " . $filepath . " nie istnieje.");
			}
			return clone self::$default_ctrl;
		}

		private function sendFeedbackMsg(Request $request, $msg = "Strona o podanym adresie nie istnieje.") {
			$request->addFeedback($msg);
		}
	}