<?php
    require_once 'framework/base/controller.php';
    require_once 'framework/base/request.php';
    require_once 'models/employees-model.php';

    class LoginController extends Controller {
        private $employee_model;

        function doExecute()
        {
            $action = $this->request->getProperty("action");
            $this->employee_model = new EmployeeModel($this->request);
            switch ($action) {
                case 'try':
                    $this->tryLogin();
                    break;
                case 'logout':
                    $this->logout();
                    break;
                default:
                    $this->showForm();
                    break;
            }
        }

        private function showForm() {
            $_SESSION['last_page'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
            include("views/login.php");
        }

        private function tryLogin() {
            $email = $this->request->getProperty('email');
            $password = $this->request->getProperty('password');
            $is_logged = $this->employee_model->login($email ,$password);
            if(!$is_logged) {
                $this->request->addFeedback('Login lub hasło niepoprawne.');
                include("views/login.php");
            } else {
                header('Location: ?page=employee&id='. $_SESSION['logged']['id']);
            }
        }

        private  function logout() {
            $this->employee_model->logout();
            $this->request->addFeedback('Wylogowano poprawnie.');
            header('Location: index.php');
        }
    }