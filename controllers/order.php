<?php
    require_once 'framework/base/controller.php';
    require_once 'framework/base/request.php';
    require_once 'models/orders-model.php';
    require_once 'models/products-model.php';

    class OrderController extends Controller {
        private $orders_model;
        private $products_model;

        function doExecute() {
            $action = $this->request->getProperty('action');
            $this->orders_model = new OrdersModel($this->request);
            $this->products_model = new ProductsModel($this->request);

            switch($action) {
                case 'addToOrder':
                    $this->addToOrder();
                    break;
                case 'showOrders':
                    $this->showOrders();
                    break;
                case 'delete':
                    $this->delete();
                    break;
                case 'save':
                    $this->save();
                    break;
                default:
                    $this->showOrder();
                    break;
            }
        }

        private function addToOrder() {
            if(!isset($_SESSION['order']) || !is_array($_SESSION['order'])) {
                $_SESSION['order'] = [];
            }
            if(!isset($_SESSION['order'][$this->request->getProperty('product')])) {
                $_SESSION['order'][$this->request->getProperty('product')] = 0;
            }
            $_SESSION['order'][$this->request->getProperty('product')] += $this->request->getProperty('amount');
            header('Location: index.php');
        }

        private function showOrders() {
            $orders = $this->orders_model->listTodayOrders();
            include('views/show-orders.php');
        }

        private function showOrder() {
            $products = $this->normalizeProducts();
            include('views/show-order.php');
        }

        private function delete() {
            $product_id = $this->request->getProperty('product_id');
            unset($_SESSION['order'][$product_id]);
            header('Location: ?page=order');
        }

        private function save() {
            $products = $this->normalizeProducts();
            $this->orders_model->add($_SESSION['logged']['id'], $products);
            header('Location: ?page=order&action=showOrders');
        }

        private function normalizeProducts() {
            $products = [];
            if(is_array($_SESSION['order'])) {
                $products = $this->products_model->listProductsWithID(array_keys($_SESSION['order']));
            }
            foreach($products as $key => $product) {
                $amount = $_SESSION['order'][$product['product_id']];
                $products[$key]['amount'] = $amount;
            }
            return $products;
        }
    }