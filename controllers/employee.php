<?php
    require_once 'framework/base/controller.php';
    require_once 'framework/base/request.php';
    require_once 'models/employees-model.php';
    require_once 'models/deliverers-model.php';
    require_once 'models/products-model.php';
    require_once 'framework/utils/validators/password-validator.php';

    class EmployeeController extends Controller {
        private $employee_model;
        private $deliverer_model;
        private $products_model;

        function doExecute() {
            $action = $this->request->getProperty('action');
            $this->employee_model = new EmployeeModel($this->request);
            $this->deliverer_model = new DeliverersModel($this->request);
            $this->products_model = new ProductsModel($this->request);
            switch($action) {
                case 'employees':
                    if($_SESSION['logged']['is_admin']) {
                        $this->employees();
                    }
                    break;
                case 'deliverers':
                    if($_SESSION['logged']['is_admin']) {
                        $this->deliverers();
                    }
                    break;
                case 'newEmployee':
                    if($_SESSION['logged']['is_admin']) {
                        $this->newEmployee();
                    }
                    break;
               case 'updateEmployee':
                   if($_SESSION['logged']['is_admin']) {
                       $this->updateEmployee();
                   }
                   break;
               case 'deleteEmployee':
                   if($_SESSION['logged']['is_admin']) {
                       $this->deleteEmployee();
                   }
                   break;
                case 'saveUpdatedEmployee':
                    if($_SESSION['logged']['is_admin']) {
                        $this->saveUpdatedEmployee();
                    }
                    break;
               case 'newDeliverer':
                   if($_SESSION['logged']['is_admin']) {
                       $this->newDeliverer();
                   }
                   break;
                case 'editDeliverer':
                    if($_SESSION['logged']['is_admin']) {
                        $this->updateDeliverer();
                    }
                    break;
                case 'saveUpdatedDeliverer':
                    if($_SESSION['logged']['is_admin']) {
                        $this->saveUpdatedDeliverer();
                    }
                    break;
                case 'deleteDeliverer':
                    if($_SESSION['logged']['is_admin']) {
                        $this->deleteDeliverer();
                    }
                    break;
                case 'showProducts':
                    if($_SESSION['logged']['is_admin']) {
                        $this->showProducts();
                    }
                    break;
                case 'newProduct':
                    if($_SESSION['logged']['is_admin']) {
                        $this->newProduct();
                    }
                    break;
                case 'editProduct':
                    if($_SESSION['logged']['is_admin']) {
                        $this->updateProduct();
                    }
                    break;
                case 'saveUpdatedProduct':
                    if($_SESSION['logged']['is_admin']) {
                        $this->saveUpdatedProduct();
                    }
                    break;
                case 'deleteProduct':
                    if($_SESSION['logged']['is_admin']) {
                        $this->deleteProduct();
                    }
                    break;
                default:
                    $this->describe();
                    break;
            }
        }

        private function describe() {
            $id = $this->request->getProperty('id');
            $action = $this->request->getProperty('action');
            $employee = $this->employee_model->getEmployee($id);
            include('views/employee.php');
        }

        private  function employees() {
            $id = $this->request->getProperty('id');
            $action = $this->request->getProperty('action');
            $employee = $this->employee_model->getEmployee($id);
            $employees = $this->employee_model->listEmployees();
            include('views/employee.php');
        }

        private function deliverers() {
            $id = $this->request->getProperty('id');
            $action = $this->request->getProperty('action');
            $employee = $this->employee_model->getEmployee($id);
            $deliverers = $this->deliverer_model->listDeliverersWithProducts();
//            var_dump($deliverers);
            include('views/employee.php');
        }

        private function newEmployee() {
            $data = [
                'first_name' => $this->request->getProperty('first_name'),
                'last_name' => $this->request->getProperty('last_name'),
                'password' => $this->generatePassword(38),
                'email' => $this->request->getProperty('email'),
                'phone' => $this->request->getProperty('phone'),
                'is_admin' => $this->request->getProperty('is_admin')
            ];

            $id = $this->employee_model->setEmployee($data);

            $_SESSION['created_user'] = $data;

            header('Location: ?page=employee&action=employees&id='. $_SESSION['logged']['id']);
        }

        private function updateEmployee() {
            $employee_id = $this->request->getProperty('employee_id');
            $employee = $this->employee_model->getEmployee($employee_id);
            include('views/update-employee.php');
        }

        private function saveUpdatedEmployee() {
            $data = [
                'first_name' => $this->request->getProperty('first_name'),
                'last_name' => $this->request->getProperty('last_name'),
                'email' => $this->request->getProperty('email'),
                'phone' => $this->request->getProperty('phone'),
                'is_admin' => $this->request->getProperty('is_admin')
            ];

            if(!$this->employee_model->updateEmployee($this->request->getProperty('employee_id'), $data)) {
                $_SESSION['error'] = true;
                header('Location: ?page=employee&action=updateEmployee&employee_id=' . $this->request->getProperty('employee_id'));
            }
            header('Location: ?page=employee&action=employees&id=' . $_SESSION['logged']['id']);
        }

        private function deleteEmployee() {
            $employee_id = $this->request->getProperty('employee_id');
            $this->employee_model->deleteEmployee($employee_id);
            header('Location: ?page=employee&action=employees&id=' . $_SESSION['logged']['id']);
        }

        private function generatePassword($entropy) {
            $alphabet = [
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'x', 'y', 'z',
                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '+', '=', '{', '}', '[', ']', ';', ':', '\'', '"', '\\', '|', '<', '>', ',', '.', '/', '?'
            ];

            $password = '';
            do {
                $password .= $alphabet[array_rand($alphabet)];
                $validator = new PasswordValidator($password);
                $validator->setMinimumLevelOfEntropy($entropy);
            } while(!$validator->check());

            return $password;
        }

        private function newDeliverer() {
            $data = [
                'name' => $this->request->getProperty('name'),
                'phone' => $this->request->getProperty('phone'),
                'www' => $this->request->getProperty('www'),
                'email' => $this->request->getProperty('email'),
                'description' => $this->request->getProperty('description'),
                'address' => $this->request->getProperty('address'),
                'zip' => $this->request->getProperty('zip'),
                'city' => $this->request->getProperty('city')
            ];

            $id = $this->deliverer_model->addDeliverer($data);

            header('Location: ?page=employee&action=deliverers&id='. $_SESSION['logged']['id']);
        }

        private function updateDeliverer() {
            $deliverer_id = $this->request->getProperty('deliverer_id');
            $deliverer = $this->deliverer_model->getDeliverer($deliverer_id);
            include('views/update-deliverer.php');
        }

        private function saveUpdatedDeliverer() {
            $data = [
                'name' => $this->request->getProperty('name'),
                'phone' => $this->request->getProperty('phone'),
                'www' => $this->request->getProperty('www'),
                'email' => $this->request->getProperty('email'),
                'description' => $this->request->getProperty('description'),
                'address' => $this->request->getProperty('address'),
                'zip' => $this->request->getProperty('zip'),
                'city' => $this->request->getProperty('city')
            ];

            if(!$this->deliverer_model->updateDeliverer($this->request->getProperty('deliverer_id'), $data)) {
                $_SESSION['error'] = true;
                header('Location: ?page=employee&action=updateDeliverer&deliverer_id=' . $this->request->getProperty('deliverer_id'));
            }
            header('Location: ?page=employee&action=deliverers&id=' . $_SESSION['logged']['id']);
        }

        private function deleteDeliverer() {
            $deliverer_id = $this->request->getProperty('deliverer_id');
            $this->deliverer_model->deleteDeliverer($deliverer_id);
            header('Location: ?page=employee&action=deliverers&id=' . $_SESSION['logged']['id']);
        }

        private function showProducts() {
            $deliverer_id = $this->request->getProperty('deliverer_id');
            $products = $this->products_model->listProductsOfDeliverer($deliverer_id);
            $deliverer = $this->deliverer_model->getDeliverer($deliverer_id);
            include('views/products.php');
        }

        private function newProduct() {
            $data = [
                'name' => $this->request->getProperty('name'),
                'description' => $this->request->getProperty('description'),
                'price' => $this->request->getProperty('price')
            ];

            $id = $this->products_model->addProduct($this->request->getProperty('deliverer_id'), $data);

            header('Location: ?page=employee&action=showProducts&deliverer_id='. $this->request->getProperty('deliverer_id'));
        }

        private function updateProduct() {
            $product_id = $this->request->getProperty('product_id');
            $product = $this->products_model->getProduct($product_id);
            include('views/update-product.php');
        }

        private function saveUpdatedProduct() {
            $data = [
                'name' => $this->request->getProperty('name'),
                'description' => $this->request->getProperty('description'),
                'deliverer_id' => $this->request->getProperty('deliverer_id'),
                'price' => $this->request->getProperty('price')
            ];

            if(!$this->products_model->updateProduct($this->request->getProperty('product_id'), $data)) {
                $_SESSION['error'] = true;
                header('Location: ?page=employee&action=editProduct&deliverer_id=' . $this->request->getProperty('deliverer_id') . '&product_id=' . $this->request->getProperty('product_id'));
            }
            header('Location: ?page=employee&action=showProducts&deliverer_id=' . $this->request->getProperty('deliverer_id'));
        }

        private function deleteProduct() {
            $product_id = $this->request->getProperty('product_id');
            $this->products_model->deleteProduct($product_id);
            header('Location: ?page=employee&action=showProducts&deliverer_id=' . $this->request->getProperty('deliverer_id'));
        }
    }