<?php
	require_once 'framework/base/controller.php';
	require_once 'framework/base/request.php';
	require_once 'models/deliverers-model.php';

	class MainPageController extends Controller {
		private $deliverers_model;

		function doExecute() {
			$action = $this->request->getProperty("action");
			$this->deliverers_model = new DeliverersModel($this->request);
			switch ($action) {
				default:
					$this->showMainPage();
					break;
			}
		}

		private function showMainPage() {
			$deliverers = $this->deliverers_model->listDeliverersWithProducts();
//			var_dump($deliverers);
			include("views/main-page.php");
		}
	}