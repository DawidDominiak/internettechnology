<?php
    require_once 'framework/base/model.php';

    class OrdersModel extends Model {
        public function add($employee_id, $products) {
            $last_inserted_order_id = null;
            try {
                $sql_for_order_adding = 'INSERT INTO `orders` (
                    employee_id
                ) VALUES (
                    :employee_id
                )';
                $stmt = $this->db->prepare($sql_for_order_adding);
                $stmt->execute([
                    'employee_id' => $employee_id
                ]);
                $last_inserted_order_id = $this->db->lastInsertId();

                $sql_for_ordered_products_adding = 'INSERT INTO `ordered_products` (
                    order_id, product_id, amount
                ) VALUES ' . join(', ', array_fill(0, count($products), '(?, ?, ?)'));
                $data = [];
                foreach($products as $product) {
                    $data[] = $last_inserted_order_id;
                    $data[] = $product['product_id'];
                    $data[] = $product['amount'];
                }
                $stmt = $this->db->prepare($sql_for_ordered_products_adding);
                $stmt->execute($data);
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
                return false;
            }
            return true;
        }

        public function listTodayOrders() {
            $orders_list = [];
            try {
                $sql_for_list_orders = 'SELECT
                    o.datetime,
                    e.first_name,
                    e.last_name,
                    e.email as employee_email,
                    e.phone as employee_phone,
                    p.name as product_name,
                    d.name as deliverer_name,
                    d.email as deliverer_email,
                    d.phone as deliverer_phone,
                    d.WWW as deliverer_www,
                    p.price,
                    op.amount,
                    (p.price * op.amount) as total_price
                FROM `orders` o
                LEFT JOIN `employees` e ON e.id = o.employee_id
                RIGHT JOIN `ordered_products` op ON op.order_id = o.id
                LEFT JOIN `products` p ON p.id = op.product_id
                LEFT JOIN `deliverers` d ON p.deliverer_id = d.id
                WHERE DATE(o.datetime) = CURDATE()
                ORDER BY d.id
                ';
                $stmt = $this->db->prepare($sql_for_list_orders);
                $stmt->execute();
                while($product = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $orders_list[] = $product;
                }
            } catch(PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $orders_list;
        }
    }