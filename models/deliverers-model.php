<?php
	require_once 'framework/base/model.php';

	class DeliverersModel extends Model {
		public function listDeliverersWithProducts($array_with_ids = false) {
			$deliverers_list = [];
			try {
				$sql_for_list_deliverers = 'SELECT
					p.id as product_id,
					d.id as deliverer_id,
					p.name as product_name,
					p.description as product_description,
					price,
					d.name as deliverer_name,
					d.phone,
					d.WWW,
					d.email,
					d.description as deliverer_description,
					d.address,
					d.zip,
					d.city
				FROM `products` p';
				$sql_for_list_deliverers .= ' RIGHT JOIN `deliverers` d ON p.deliverer_id = d.id';

				$if_all = !is_array($array_with_ids);

				if(!$if_all) {
					$sql_for_list_deliverers .= ' WHERE p.deliverer_id IN (';
					$temp_array = [];
					for($i = 0; $i < count($array_with_ids); $i++) {
						array_push($temp_array, '?');
					}
					$sql_for_list_deliverers .= join($temp_array, ', ');
					$sql_for_list_deliverers .= ')';
				}

				$stmt = $this->db->prepare($sql_for_list_deliverers);
				if(!$if_all) {
					$temp_array = [];
					foreach($array_with_ids as $id) {
						array_push($temp_array, $id);
					}
					$stmt->execute($temp_array);
				} else {
					$stmt->execute();
				}
				while($product = $stmt->fetch()) {
					if(!isset($deliverers_list[$product['deliverer_id']])) {
						$deliverers_list[$product['deliverer_id']] = [
							'id' => $product['deliverer_id'],
							'name' => $product['deliverer_name'],
							'phone' => $product['phone'],
							'www' => $product['WWW'],
							'email' => $product['email'],
							'description' => $product['deliverer_description'],
							'address' => $product['address'],
							'zip' => $product['zip'],
							'city' => $product['city'],
							'products' => []
						];
					}
					if($product['product_id']) {
						array_push($deliverers_list[$product['deliverer_id']]['products'], [
							'id' => $product['product_id'],
							'name' => $product['product_name'],
							'description' => $product['product_description'],
							'price' => $product['price']
						]);
					}
				}
			} catch(PDOException $exception) {
				$this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
			}

			return $deliverers_list;
		}

		public function getDeliverer($id) {
			$deliverer = null;
			try {
				$sql_for_get_deliverer = 'SELECT * FROM `deliverers` WHERE id = :id LIMIT 1';
				$stmt = $this->db->prepare($sql_for_get_deliverer);
				$stmt->execute([
					'id' => $id
				]);
				$deliverer = $stmt->fetch();
			} catch (PDOException $exception) {
				$this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
			}
			return $deliverer;
		}

		public function addDeliverer($data) {
			$last_inserted_deliverer_id = null;
			try {
				$sql_for_deliverer_creation = 'INSERT INTO `deliverers` (
					name, phone, WWW, email, description, address, zip, city
				) VALUES (
					:name, :phone, :www, :email, :description, :address, :zip, :city
				)';
				$stmt = $this->db->prepare($sql_for_deliverer_creation);
				$stmt->execute([
					'name' => $data['name'] ? $data['name'] : null,
					'phone' => $data['phone'] ? $data['phone'] : null,
					'www' => $data['www'] ? $data['www'] : null,
					'email' => $data['email'] ? $data['email'] : null,
					'description' => $data['description'] ? $data['description'] : null,
					'address' => $data['address'] ? $data['address'] : null,
					'zip' => $data['zip'] ? $data['zip'] : null,
					'city' => $data['city'] ? $data['city'] : null,
				]);
				$last_inserted_deliverer_id = $this->db->lastInsertId();
			} catch (PDOException $exception) {
				$this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
			}
			return $last_inserted_deliverer_id;
		}

		public function updateDeliverer($id, $data) {
			try {
				$sql_for_deliverer_update = 'UPDATE `deliverers` SET
					name = :name,
					phone = :phone,
					WWW = :www,
					email = :email,
					description = :description,
					address = :address,
					zip = :zip,
					city = :city
				WHERE id = :id
				LIMIT 1';
				$stmt = $this->db->prepare($sql_for_deliverer_update);
				return $stmt->execute([
					'id' => $id,
					'name' => $data['name'] ? $data['name'] : null,
					'phone' => $data['phone'] ? $data['phone'] : null,
					'www' => $data['www'] ? $data['www'] : null,
					'email' => $data['email'] ? $data['email'] : null,
					'description' => $data['description'] ? $data['description'] : null,
					'address' => $data['address'] ? $data['address'] : null,
					'zip' => $data['zip'] ? $data['zip'] : null,
					'city' => $data['city'] ? $data['city'] : null
				]);
			} catch (PDOException $exception) {
				$this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
				return false;
			}
		}

		public function deleteDeliverer($id) {
			try {
				$sql_for_deleting_deliverer = 'DELETE FROM `deliverers`
					WHERE id = :id
					LIMIT 1
				';
				$stmt = $this->db->prepare($sql_for_deleting_deliverer);
				$stmt->execute([
					'id' => $id
				]);
			} catch (PDOException $exception) {
				$this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
				return false;
			}
			return true;
		}
	}