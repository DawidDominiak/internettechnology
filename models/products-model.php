<?php
    require_once 'framework/base/model.php';

    class ProductsModel extends Model {
        public function listProductsOfDeliverer($deliverer_id) {
            $products_list = [];
            try {
                $sql_for_list_products = ' SELECT * FROM `products`
                    WHERE deliverer_id = :id
                ';
                $stmt = $this->db->prepare($sql_for_list_products);
                $stmt->execute([
                    'id' => $deliverer_id
                ]);
                while($product = $stmt->fetch()) {
                    $products_list[] = $product;
                };
            } catch(PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $products_list;
        }

        public function listProductsWithID($array_with_ids) {
            $products_list = [];
            try {
                $sql_for_list_products = 'SELECT
                    p.id as product_id,
                    d.id as deliverer_id,
                    p.name as product_name,
                    d.name as deliverer_name,
                    p.description as product_description,
                    d.description as deliverer_description,
                    p.price,
                    d.phone,
                    d.WWW as www
                FROM `products` p
                LEFT JOIN `deliverers` d ON d.id = p.deliverer_id
                WHERE p.id IN (' . join(', ', array_fill(0, count($array_with_ids), '?')) . ')';
                $stmt = $this->db->prepare($sql_for_list_products);
                $stmt->execute($array_with_ids);
                while($product = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $products_list[] = $product;
                }
            } catch(PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $products_list;
        }

        public function getProduct($id) {
            $product = null;
            try {
                $sql_for_get_product = 'SELECT * FROM `products` WHERE id = :id LIMIT 1';
                $stmt = $this->db->prepare($sql_for_get_product);
                $stmt->execute([
                    'id' => $id
                ]);
                $product = $stmt->fetch();
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $product;
        }

        public function addProduct($deliverer_id, $data) {
            $last_inserted_product_id = null;
            try {
                $sql_for_product_adding = 'INSERT INTO `products` (
                    deliverer_id, name, description, price
                ) VALUES (
                    :deliverer_id, :name, :description, :price
                )';
                $stmt = $this->db->prepare($sql_for_product_adding);
                $stmt->execute([
                    'deliverer_id' => $deliverer_id,
                    'name' => $data['name'],
                    'description' => $data['description'],
                    'price' => $data['price']
                ]);
                $last_inserted_product_id = $this->db->lastInsertId();
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $last_inserted_product_id;
        }

        public function updateProduct($id, $data) {
            try {
                $sql_for_product_update = 'UPDATE `products` SET
                    deliverer_id = :deliverer_id,
                    name = :name,
                    description = :description,
                    price = :price
                WHERE id = :id
                LIMIT 1';
                $stmt = $this->db->prepare($sql_for_product_update);
                $stmt->execute([
                    'id' => $id,
                    'deliverer_id' => $data['deliverer_id'],
                    'name' => $data['name'],
                    'description' => $data['description'],
                    'price' => $data['price']
                ]);
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
                return false;
            }
            return true;
        }

        public function deleteProduct($id) {
            try {
                $sql_for_deleting_product = 'DELETE FROM `products`
					WHERE id = :id
					LIMIT 1
				';
                $stmt = $this->db->prepare($sql_for_deleting_product);
                $stmt->execute([
                    'id' => $id
                ]);
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
                return false;
            }
            return true;
        }
    }