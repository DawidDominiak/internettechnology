<?php
    require_once 'framework/base/model.php';

    class EmployeeModel extends Model {

        public function listEmployees() {
            $list = [];
            $users = [];
            try {
                $sql_for_get_employees_data = 'SELECT * FROM `employees`';
                $stmt = $this->db->prepare($sql_for_get_employees_data);
                $results = $stmt->execute();
                $i = 0;
                while($list[$i] = $stmt->fetch()) {
                    $users[$i] = [
                        'id' => $list[$i]['id'],
                        'first_name' => $list[$i]['first_name'],
                        'last_name' => $list[$i]['last_name'],
                        'email' => $list[$i]['email'],
                        'phone' => $list[$i]['phone'],
                        'is_admin' => ($list[$i]['is_admin'] === '1')
                    ];
                    $i++;
                };
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $users;
        }

        public function getEmployee($id) {
            $employee = null;
            try {
                $sql_for_get_employee = 'SELECT * FROM `employees` WHERE id = :id LIMIT 1';
                $stmt = $this->db->prepare($sql_for_get_employee);
                $stmt->execute([
                    'id' => $id
                ]);
                $employee = $stmt->fetch();
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $employee;
        }

        public function setEmployee($employee_data) {
            $last_inserted_user_id = null;
            try {
                $sql_for_employee_creation = 'INSERT INTO `employees` (
                    first_name, last_name, password, email, phone, is_admin
                ) VALUES (
                    :first_name, :last_name, :password, :email, :phone, :is_admin
                )';
                $stmt = $this->db->prepare($sql_for_employee_creation);
                $stmt->execute([
                    'first_name' => $employee_data['first_name'],
                    'last_name' => $employee_data['last_name'],
                    'password' => $this->hashPassword($employee_data['password']),
                    'email' => $employee_data['email'],
                    'phone' => $employee_data['phone'],
                    'is_admin' => ($employee_data['is_admin'] === 'true')
                ]);
                $last_inserted_user_id = $this->db->lastInsertId();
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
            return $last_inserted_user_id;
        }

        public function updateEmployee($id, $employee_data) {
            try {
                $sql_for_employee_update = 'UPDATE `employees` SET
                    first_name = :first_name,
                    last_name = :last_name,
                    email = :email,
                    phone = :phone,
                    is_admin = :is_admin
                WHERE id = :id
                LIMIT 1';
                $stmt = $this->db->prepare($sql_for_employee_update);
                $stmt->execute([
                    'id' => $id,
                    'first_name' => $employee_data['first_name'],
                    'last_name' => $employee_data['last_name'],
                    'email' => $employee_data['email'],
                    'phone' => $employee_data['phone'],
                    'is_admin' => ($employee_data['is_admin'] === 'true')
                ]);
            } catch (PDOException $exception) {
                echo $exception->getMessage();
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
                return false;
            }
            return true;
        }

        public function deleteEmployee($id) {
            try {
                $sql_for_deleting_employee = 'DELETE FROM `employees`
                    WHERE id = :id
                    LIMIT 1';
                $stmt = $this->db->prepare($sql_for_deleting_employee);
                $stmt->execute([
                    'id' => $id
                ]);
            } catch (PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
                return false;
            }
            return true;
        }

        public function login($email, $password) {
            try {
                $sql_for_login = 'SELECT id, first_name, last_name, phone, is_admin, COUNT(*) as is_correct FROM `employees` WHERE email = :email AND password = :password';
                $stmt = $this->db->prepare($sql_for_login);
                $results = $stmt->execute([
                    'email' => $email,
                    'password' => $this->hashPassword($password)
                ]);
                $employee = $stmt->fetch();
                $is_correct = $employee['is_correct'] > 0 ? true : false;
                if($is_correct) {
                    $_SESSION['logged'] = [
                        'value' => true,
                        'email' => $email,
                        'id' => $employee['id'],
                        'first_name' => $employee['first_name'],
                        'last_name' => $employee['last_name'],
                        'phone' => $employee['phone'],
                        'is_admin' => $employee['is_admin']
                    ];
                }
                return $is_correct;
            } catch(PDOException $exception) {
                $this->request->addFeedback('Połączenie z bazą danych nie mogło zostać w tym momencie zrealizowane. ' . $exception->getMessage());
            }
        }

        public function logout() {
            unset($_SESSION['logged']);
        }
    }