<!doctype html>
<html>
    <?php include('views/elements/header.php'); ?>
    <body class="container">
        <div class="col-sm-9">
            <?php if($this->request->hasFeedback()) { ?>
                <ul class="alert alert-warning">
                    <?php
                    $feedback_messages = $this->request->getFeedback();
                    foreach($feedback_messages as $message) {
                        ?>
                        <li><?=$message?></li>
                    <?php }	?>
                </ul>
            <?php } ?>
            <h1>Zamów jedzenie</h1>
            <?php if(!isset($_SESSION['logged'])) { ?>
                <a href="?page=login" class="btn btn-info" role="button">Zaloguj</a>
            <?php } else { ?>
                Zalogowany
                <a href="?page=login&amp;action=logout" class="btn btn-info" role="button">Wyloguj</a>
                <a href="?page=employee" class="btn btn-info" role="button">Twój panel</a>
            <?php } ?>
            <a href="?page=order" class="btn btn-info" role="button">Twoje zamówienie (<?=isset($_SESSION['order']) ? count($_SESSION['order']) : 0 ?>)</a>
            <?php
                if(is_array($deliverers)) {
                    foreach($deliverers as $deliverer) {
            ?>
                <h2><?=$deliverer['name'] ?> <small>Produkty dostawcy</small></h2>
                <table class="table table-hover">
                    <tr>
                        <th>Nazwa</th>
                        <th>Opis</th>
                        <th>Cena</th>
                        <th>Ilość</th>
                        <th>Do zamówienia</th>
                    </tr>
                    <?php
                        foreach($deliverer['products'] as $product) {
                    ?>
                        <tr data-product="<?=$product['id'] ?>">
                            <td>
                                <?=$product['name'] ?>
                            </td>
                            <td>
                                <?=$product['description'] ?>
                            </td>
                            <td>
                                <?=$product['price'] ?>
                            </td>
                            <td>
                                <input type="text" size="2" value="1" onchange="document.querySelector('tr[data-product=\'<?=$product['id'] ?>\'] a').href = '?page=order&action=addToOrder&product=<?=$product['id'] ?>&amount=' + this.value;">
                            </td>
                            <td>
                                <a href="?page=order&amp;action=addToOrder&amp;product=<?=$product['id'] ?>&amp;amount=1">Dodaj</a>
                            </td>
                        </tr>
                    <?php
                        }
                    ?>
                </table>
            <?php
                    }
                }
            ?>
        </div>
    </body>
</html>