<!doctype html>
<html>
<?php include('views/elements/header.php'); ?>
<body class="container">
<div class="row">
    <?php if($this->request->hasFeedback()) { ?>
        <ul class="alert alert-warning">
            <?php
            $feedback_messages = $this->request->getFeedback();
            foreach($feedback_messages as $message) {
                ?>
                <li><?=$message?></li>
            <?php }	?>
        </ul>
    <?php } ?>

    <h2>Edytuj dostawcę <?=$deliverer['name'] ?></h2>
    <form class="form-horizontal" action="?page=employee&amp;action=saveUpdatedDeliverer&amp;deliverer_id=<?=$deliverer['id'] ?>" method="post">
        <fieldset>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Imię</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="name" name="name" value="<?=$deliverer['name'] ?>" placeholder="Nazwa" required>
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                    <input class="form-control" type="tel" id="phone" name="phone" value="<?=$deliverer['phone'] ?>" placeholder="Telefon">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">WWW</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="www" name="www" value="<?=$deliverer['WWW'] ?>" placeholder="WWW">
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                    <input class="form-control" type="email" id="email" name="email" value="<?=$deliverer['email'] ?>" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Opis</label>
                <div class="col-sm-10">
                    <textarea name="description" name="description" placeholder="Opis" cols="100" rows="8"><?=$deliverer['description'] ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Adres</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="address" name="address" value="<?=$deliverer['address'] ?>" placeholder="Adres">
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Kod pocztowy</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="zip" name="zip" value="<?=$deliverer['zip'] ?>" placeholder="Kod">
                </div>
            </div>
            <div class="form-group">
                <label for="first_name" class="col-sm-2 control-label">Miasto</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="city" name="city" value="<?=$deliverer['city'] ?>" placeholder="Miasto">
                </div>
            </div>
            <div class="form-group">
                <input class="form-control" type="submit" value="Zapisz">
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>