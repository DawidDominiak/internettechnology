<!doctype html>
<html>
<?php include('views/elements/header.php'); ?>
<body class="container">
<div class="row">
    <h1>Dzisiejsze zamówienia</h1>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="index.php">Strona główna</a></li>
    </ul>
    <h2>Lista produktów do zamówienia dzisiaj</h2>
    <?php
    if(count($orders) === 0) {
        ?>
        <p>Brak zamówień.</p>
    <?php
    } else {
        ?>
        <table class="table table-hover">
            <tr>
                <th>Dostawca</th>
                <th>Produkt</th>
                <th>Kontakt do dostawcy</th>
                <th>Pracownik</th>
                <th>Kontakt do pracownika</th>
                <th>Cena jedn.</th>
                <th>Ilość</th>
                <th>Łącznie</th>
            </tr>
            <?php
            foreach($orders as $order) {
                ?>
                <tr>
                    <td><?=$order['deliverer_name'] ?></td>
                    <td><?=$order['product_name'] ?></td>
                    <td>tel. <?=$order['deliverer_phone'] ?>, <a href="mailto:<?=$order['deliverer_email'] ?>"><?=$order['deliverer_email'] ?></a>, <a href="<?=$order['deliverer_www'] ?>">www</a></td>
                    <td><?=$order['first_name'] ?> <?=$order['last_name'] ?></td>
                    <td><?=$order['employee_phone'] ?>, <a href="mailto:<?=$order['employee_email'] ?>">email</a></td>
                    <td><?=$order['price'] ?></td>
                    <td><?=$order['amount'] ?></td>
                    <td><?=$order['total_price'] ?></td>
                </tr>
            <?php
            }
            ?>
        </table>
    <?php
    }
    ?>
</div>
</body>
</html>