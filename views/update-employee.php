<!doctype html>
<html>
    <?php include('views/elements/header.php'); ?>
    <body class="container">
        <div class="row">
            <?php if($this->request->hasFeedback()) { ?>
                <ul class="alert alert-warning">
                    <?php
                    $feedback_messages = $this->request->getFeedback();
                    foreach($feedback_messages as $message) {
                        ?>
                        <li><?=$message?></li>
                    <?php }	?>
                </ul>
            <?php } ?>

            <h2>Edytuj pracownika</h2>
            <form class="form-horizontal" action="?page=employee&amp;action=saveUpdatedEmployee&amp;employee_id=<?=$employee['id'] ?>" method="post">
                <fieldset>
                    <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label">Imię</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="first_name" name="first_name" value="<?=$employee['first_name'] ?>" placeholder="Imię" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-2 control-label">Nazwisko</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" id="last_name" name="last_name" value="<?=$employee['last_name'] ?>" placeholder="Nazwisko" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="email" id="email" name="email" value="<?=$employee['email'] ?>" placeholder="Email" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-sm-2 control-label">Phone</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="tel" id="phone" name="phone" value="<?=$employee['phone'] ?>" placeholder="Telefon" required>
                        </div>
                    </div>
                    <div class="col-sm-offset-2 col-sm-10">
                        <p><strong>Czy ma mieć uprawnienia administratora?</strong></p>
                        <div class="checkbox">
                            <label><input type="checkbox" name="is_admin" value="true" <?php if($employee['is_admin'] === '1') { ?> checked="checked"<?php } ?>> admin</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="submit" value="Zapisz">
                    </div>
                </fieldset>
            </form>
        </div>
    </body>
</html>