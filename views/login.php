<!doctype html>
<html>
	<?php include('views/elements/header.php');?>
	<body class="container">
		<div class="row">
			<?php if($this->request->hasFeedback()) { ?>
				<ul class="alert alert-warning">
					<?php 
						$feedback_messages = $this->request->getFeedback();
						foreach($feedback_messages as $message) { 
					?>
						<li><?=$message?></li>
					<?php }	?>
				</ul>
			<?php } ?>
		</div>
		<h1>Formularz logowania</h1>
		<form class="form-horizontal" action="?page=login&amp;action=try&amp;rand=<?= rand(); ?>" method="post">
			<fieldset>
				<div class="form-group">
					<label for="login" class="col-sm-2 control-label">Email</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" id="email" name="email" placeholder="Email" <?php $value = $this->request->getProperty("email"); if($value) echo 'value="' . $value . '"'; ?> required>
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Hasło</label>
					<div class="col-sm-10">
						<input class="form-control" type="password" id="password" name="password" placeholder="Hasło" required>
					</div>
				</div>
				<div class="form-group">
					<input class="form-control" type="submit" value="Zapisz">
				</div>
			</fieldset>
		</form>
		<a role="button" class="btn btn-primary" href="index.php">Powrót do strony głównej</a>
		<a role="button" class="btn btn-default" href="<?= $_SESSION['last_page']; ?>">Powrót do ostatniej</a>
		<?php include('views/elements/scripts.php');?>
	</body>
</html>