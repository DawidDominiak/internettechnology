<!doctype html>
<html>
<?php include('views/elements/header.php'); ?>
<body class="container">
<div class="row">
    <h1>Twoje zamówienie</h1>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="index.php">Strona główna</a></li>
    </ul>
    <h2>Lista produktów</h2>
    <?php
    if(count($products) === 0) {
        ?>
        <p>Wybierz produkty.</p>
    <?php
    } else {
        ?>
        <table class="table table-hover">
            <tr>
                <th>Nazwa</th>
                <th>Dostawca</th>
                <th>Opis</th>
                <th>Cena jedn.</th>
                <th>Ilość</th>
                <th>Cena łączenie</th>
                <th>Usuń</th>
            </tr>
            <?php
            $sum = 0;
            foreach($products as $product) {
                $local_sum = $product['amount'] * $product['price'];
                $sum += $local_sum;
                ?>
                <tr>
                    <td><?=$product['product_name'] ?></td>
                    <td><?=$product['deliverer_name'] ?></td>
                    <td><?=$product['product_description'] ?></td>
                    <td><?=$product['price'] ?></td>
                    <td><?=$product['amount'] ?></td>
                    <td><?=number_format($sum, 2) ?></td>
                    <td><a href="?page=order&amp;action=delete&amp;product_id=<?=$product['product_id'] ?>">Usuń</a></td>
                </tr>
            <?php
            }
            ?>
        </table>
        <p>Łącznie do zapłaty: <?=number_format($sum, 2) ?> zł.</p>
        <?php
            if(isset($_SESSION['logged'])) {
        ?>
            <a href="?page=order&amp;action=save" class="btn btn-info" role="button">Zamawiam</a>
        <?php
            } else {
        ?>
            <p><a href="?page=login">Zaloguj się</a> aby dokonać zamówienia</p>
        <?php
            }
        ?>
    <?php
    }
    ?>
</div>
</body>
</html>