<!doctype html>
<html>
<?php include('views/elements/header.php'); ?>
<body class="container">
<div class="row">
    <?php if($this->request->hasFeedback()) { ?>
        <ul class="alert alert-warning">
            <?php
            $feedback_messages = $this->request->getFeedback();
            foreach($feedback_messages as $message) {
                ?>
                <li><?=$message?></li>
            <?php }	?>
        </ul>
    <?php } ?>

    <h2>Edytuj produkt</h2>
    <form class="form-horizontal" action="?page=employee&amp;action=saveUpdatedProduct&amp;product_id=<?=$product['id'] ?>&amp;deliverer_id=<?=$this->request->getProperty('deliverer_id') ?>" method="post">
        <fieldset>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Nazwa</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="name" name="name" placeholder="Nazwa" value="<?=$product['name'] ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Opis</label>
                <div class="col-sm-10">
                    <textarea id="description" name="description" placeholder="Opis" cols="100" rows="8"><?=$product['description'] ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="price" class="col-sm-2 control-label">Cena</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="price" name="price" placeholder="Cena" value="<?=$product['price'] ?>" required>
                </div>
            </div>
            <div class="form-group">
                <input class="form-control" type="submit" value="Zapisz">
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>