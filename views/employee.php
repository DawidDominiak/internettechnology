<!doctype html>
<html>
    <?php include('views/elements/header.php'); ?>
    <body class="container">
        <div class="row">
            <?php if($this->request->hasFeedback()) { ?>
                <ul class="alert alert-warning">
                    <?php
                        $feedback_messages = $this->request->getFeedback();
                        foreach($feedback_messages as $message) {
                    ?>
                        <li><?=$message?></li>
                    <?php }	?>
                </ul>
            <?php } ?>
            <h1><?=$employee['first_name'] ? $employee['first_name'] : 'Brak' ?> <?=$employee['last_name'] ? $employee['last_name'] : 'Brak' ?></h1>
            <ul class="nav nav-tabs">
                <li role="presentation" <?php if($action == '') { ?>class="active" <?php } ?>><a href="?page=employee&amp;id=<?=$id ?>">Twój profil</a></li>
                <?php if($_SESSION['logged']['is_admin']) { ?><li role="presentation" <?php if($action == 'employees') { ?>class="active" <?php } ?>><a href="?page=employee&amp;action=employees&amp;id=<?=$id ?>">Pracownicy</a></li><?php } ?>
                <?php if($_SESSION['logged']['is_admin']) { ?><li role="presentation" <?php if($action == 'deliverers') { ?>class="active" <?php } ?>><a href="?page=employee&amp;action=deliverers&amp;id=<?=$id ?>">Dostawcy</a></li><?php } ?>
                <li role="presentation"><a href="index.php">Strona główna</a></li>
            </ul>
            <?php
            switch($action) {
                case 'employees':
                    include ('views/elements/employees.php');
                    break;
                case 'deliverers':
                    include ('views/elements/deliverers.php');
                    break;
                default:
                    include ('views/elements/profile.php');
                    break;
            }
            ?>
        </div>
    </body>
</html>