<!doctype html>
<html>
<?php include('views/elements/header.php'); ?>
<body class="container">
<div class="row">
    <?php if($this->request->hasFeedback()) { ?>
        <ul class="alert alert-warning">
            <?php
            $feedback_messages = $this->request->getFeedback();
            foreach($feedback_messages as $message) {
                ?>
                <li><?=$message?></li>
            <?php }	?>
        </ul>
    <?php } ?>
    <h1>Dostawca <?=$deliverer['name'] ?></h1>
    <ul class="nav nav-tabs">
        <li role="presentation"><a href="?page=employee&amp;action=deliverers&amp;id=<?=$_SESSION['logged']['id'] ?>">Powrót do dostawców</a></li>
    </ul>
    <h2>Lista produktów</h2>
    <?php
        if(!$products) {
    ?>
    <p>Ten dostawca nie ma na ten moment żadnego produktu.</p>
    <?php
        } else {
    ?>
    <table class="table table-hover">
        <tr>
            <th>Nazwa</th>
            <th>Opis</th>
            <th>Cena</th>
            <th>Edytuj</th>
            <th>Usuń</th>
        </tr>
    <?php
            foreach($products as $product) {
    ?>
        <tr>
            <td><?=$product['name'] ?></td>
            <td><?=$product['description'] ?></td>
            <td><?=$product['price'] ?></td>
            <td><a href="?page=employee&amp;action=editProduct&amp;deliverer_id=<?=$this->request->getProperty('deliverer_id') ?>&amp;product_id=<?=$product['id'] ?>">Edytuj</a></td>
            <td><a href="?page=employee&amp;action=deleteProduct&amp;deliverer_id=<?=$this->request->getProperty('deliverer_id') ?>&amp;product_id=<?=$product['id'] ?>">Usuń</a></td>
        </tr>
    <?php
            }
    ?>
    </table>
    <?php
        }
    ?>
    <h2>Dodaj nowy produkt dla tego dostawcy</h2>
    <form class="form-horizontal" action="?page=employee&amp;action=newProduct&amp;deliverer_id=<?=$this->request->getProperty('deliverer_id') ?>" method="post">
        <fieldset>
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Nazwa</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="name" name="name" placeholder="Nazwa" required>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Opis</label>
                <div class="col-sm-10">
                    <textarea id="description" name="description" placeholder="Opis" cols="100" rows="8"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="price" class="col-sm-2 control-label">Cena</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="price" name="price" placeholder="Cena" required>
                </div>
            </div>
            <div class="form-group">
                <input class="form-control" type="submit" value="Zapisz">
            </div>
        </fieldset>
    </form>
</div>
</body>
</html>