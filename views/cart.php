!doctype html>
<html>
	<?php include('views/elements/header.php');?>
	<body class="container">
		<div class="row">
			<table class="table">
				<tr>
					<th>Nazwa</th><th>Cena</th><th>Usuń</th>
				</tr>
				<?php foreach ($products as $product) { ?>
					<tr>
						<td><?=$product['name'] ?></td>
						<td><?=$product['price'] ?></td>
						<td><a role="button" class="btn btn-danger" href="?action=removeFromCart&amp;product=<?=$product['id'] ?>">Usuń</a></td>
					</tr>
				<?php } ?>
			</table>
		</div>
		<div class="row">
			<p>Łącznie do zapłaty: <?=$price ?> zł</p>
		</div>
		<div class="row">
			<a role="button" class="btn btn-default" href="index.php">Powrót do zakupów</a>
			<a role="button" class="btn btn-success" href="?page=order">Zamów</a>
		</div>
		<?php include('views/elements/scripts.php'); ?>
	</body>
</html>