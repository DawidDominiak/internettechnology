<h2>Dostawcy</h2>
<table class="table table-hover">
    <tr>
        <th>Nazwa</th>
        <th>Telefon</th>
        <th>www</th>
        <th>Email</th>
        <th>Opis</th>
        <th>Adres</th>
        <th>Edytuj</th>
        <th>Usuń</th>
    </tr>
    <?php foreach($deliverers as $deliverer) { ?>
    <tr>
        <td><a href="?page=employee&amp;action=showProducts&amp;deliverer_id=<?=$deliverer['id'] ?>" data-toggle="tooltip" data-placement="right" title="Kliknij aby edytować produkty dostawcy."><?=$deliverer['name'] ?></a></td>
        <td><?=$deliverer['phone'] ?></td>
        <td><?=$deliverer['www'] ?></td>
        <td><?=$deliverer['email'] ?></td>
        <td><?=$deliverer['description'] ?></td>
        <td><?=$deliverer['address'] ?> <?=$deliverer['zip'] ?> <?=$deliverer['city'] ?></td>
        <td><a href="?page=employee&amp;action=editDeliverer&amp;deliverer_id=<?=$deliverer['id'] ?>">Edytuj</a></td>
        <td><a href="?page=employee&amp;action=deleteDeliverer&amp;deliverer_id=<?=$deliverer['id'] ?>">Usuń</a></td>
    </tr>
    <?php } ?>
</table>
<h2>Dodaj nowego dostawcę</h2>
<form class="form-horizontal" action="?page=employee&amp;action=newDeliverer" method="post">
    <fieldset>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Nazwa</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="name" name="name" placeholder="Nazwa" required>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label">Telefon</label>
            <div class="col-sm-10">
                <input class="form-control" type="tel" id="phone" name="phone" placeholder="Telefon" required>
            </div>
        </div>
        <div class="form-group">
            <label for="www" class="col-sm-2 control-label">WWW</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="www" name="www" placeholder="Adres www">
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input class="form-control" type="email" id="email" name="email" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Opis</label>
            <div class="col-sm-10">
                <textarea id="description" name="description" placeholder="Opis" cols="100" rows="8"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="address" class="col-sm-2 control-label">Adres</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="address" name="address" placeholder="Adres">
            </div>
        </div>
        <div class="form-group">
            <label for="zip" class="col-sm-2 control-label">Kod pocztowy</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="zip" name="zip" placeholder="Kod">
            </div>
        </div>
        <div class="form-group">
            <label for="city" class="col-sm-2 control-label">Miasto</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="city" name="city" placeholder="Miasto">
            </div>
        </div>
        <div class="form-group">
            <input class="form-control" type="submit" value="Zapisz">
        </div>
    </fieldset>
</form>