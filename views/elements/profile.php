<h2>Twój profil</h2>
<table class="table table-hover">
                <tr>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Email</th>
                    <th>Telefon</th>
                    <?php if($_SESSION['logged']['is_admin']) { ?>
    <th>Admin</th>
<?php } ?>
</tr>
<tr>
    <td><?=$employee['first_name'] ? $employee['first_name'] : 'Brak' ?></td>
    <td><?=$employee['last_name'] ? $employee['last_name'] : 'Brak' ?></td>
    <td><a href="mailto://<?=$employee['email'] ?>"><?=$employee['email'] ?></a></td>
    <td><?=$employee['phone'] ? $employee['phone'] : 'Brak' ?></td>
    <?php if($_SESSION['logged']['is_admin']) { ?>
        <td class="success">tak</td>
    <?php } ?>
</tr>
</table>