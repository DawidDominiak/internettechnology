<h2>Aktualni pracownicy</h2>
<?php if(isset($_SESSION['created_user'])) { ?>
    <div class="bg-info">Utworzono użytkownika <?=$_SESSION['created_user']['first_name'] ?> <?=$_SESSION['created_user']['last_name'] ?>. Nowe hasło dla tego użytkownika to <?=$_SESSION['created_user']['password'] ?> .</div>
<?php } ?>
<table class="table table-hover">
    <tr>
        <th>Imię</th>
        <th>Nazwisko</th>
        <th>Email</th>
        <th>Telefon</th>
        <th>Admin</th>
        <th>Edytuj</th>
        <th>Usuń</th>
    </tr>
    <?php foreach($employees as $employee) { ?>
    <tr>
        <td><?=$employee['first_name'] ? $employee['first_name'] : 'Brak' ?></td>
        <td><?=$employee['last_name'] ? $employee['last_name'] : 'Brak' ?></td>
        <td><a href="mailto://<?=$employee['email'] ?>"><?=$employee['email'] ?></a></td>
        <td><?=$employee['phone'] ? $employee['phone'] : 'Brak' ?></td>
        <td<?php if($employee['is_admin']) { ?> class="success" <?php } ?>><?=$employee['is_admin'] ? 'tak' : 'nie' ?></td>
        <td><a href="?page=employee&amp;action=updateEmployee&amp;employee_id=<?=$employee['id'] ?>">Edytuj</a></td>
        <td><a href="?page=employee&amp;action=deleteEmployee&amp;employee_id=<?=$employee['id'] ?>">Usuń</a></td>
    </tr>
    <?php } ?>
</table>
<h2>Dodaj nowego pracownika</h2>
<form class="form-horizontal" action="?page=employee&amp;action=newEmployee" method="post">
    <fieldset>
        <div class="form-group">
            <label for="first_name" class="col-sm-2 control-label">Imię</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="first_name" name="first_name" placeholder="Imię" required>
            </div>
        </div>
        <div class="form-group">
            <label for="last_name" class="col-sm-2 control-label">Nazwisko</label>
            <div class="col-sm-10">
                <input class="form-control" type="text" id="last_name" name="last_name" placeholder="Nazwisko" required>
            </div>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input class="form-control" type="email" id="email" name="email" placeholder="Email" required>
            </div>
        </div>
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-10">
                <input class="form-control" type="tel" id="phone" name="phone" placeholder="Telefon" required>
            </div>
        </div>
        <div class="col-sm-offset-2 col-sm-10">
            <p><strong>Czy ma mieć uprawnienia administratora?</strong></p>
            <div class="checkbox">
                <label><input type="checkbox" name="is_admin" value="true"> admin</label>
            </div>
        </div>
        <div class="form-group">
            <input class="form-control" type="submit" value="Zapisz">
        </div>
    </fieldset>
</form>