<?php
	require_once 'framework/front-controller.php';
	require_once 'framework/base/request.php';

    session_start(); 
	$request = new Request();
	$front_controller = new FrontController();
	$controller = $front_controller->getCommand($request);
	$controller->execute($request);
    session_write_close(); 